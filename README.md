# Starter Gulpfile

This Gulpfile is an initial starting point for using some build tools
straight out of the box. The following has been implemented into the 
current version:

- SCSS compilation
- ES2015 JS compilation (with Babel)
- Auto-prefixing styles
- Source map generation
- Concatenating/compressing compiled files (CSS, JS)

The following is going to be implemented in later development:

- Image compression/minification
- Language linting/hinting (SCSS, JS)

## Installation

To use the build tools, all you have to do is download both the
`Gulpfile.js` and `package.json` to your HTML build, WordPress 
theme, etc. and run the `npm install` command in the same directory.

## Commands

All commands should be run in the following format:

```
gulp <command> [--arg...]
```

| Command   | Description |
|-----------|-------------|
| `compile` | Compile all assets in the build |
| `compile:styles` | Compile only the build styles |
| `compile:scripts` | Compile only the build scripts |
| `optimise:images` | Optimise any images in the specified directory |
| `watch` | Watch any source files and compile on change |
| `watch:styles` | Watch only style source files |
| `watch:scripts` | Watch only script source files |
| `watch:images` | Watch only image files |

## Compression

You can compile the scripts and styles in the build to a minified version
by passing the `--compress` argument, like so:

```
gulp compile --compress
```