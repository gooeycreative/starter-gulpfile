"use strict";

const gulp = require('gulp-help')(require('gulp'));
const watch = require('gulp-watch');

const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const imageop = require('gulp-image-optimization');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

const notifier = require('node-notifier');
const colors = require('colors');
const util = require('gulp-util');
const argv = require('yargs').argv;

const options = {
    styles: {
        entry: '*.scss',
        build: '',
        name:  'style.css',
        watch: '**/*.scss'
    },
    scripts: {
        entry: 'js/src/**/*.js',
        build: 'js/',
        name:  'main.js'
    },
    images: {
        path: 'img/',
        formats: ['png', 'gif', 'jpg', 'jpeg'],
        options: {
            progressive: true,
            optimizationLevel: 5,
            interlaced: true
        }
    },
    maps: {
        build: 'sourcemaps/'
    }
};

gulp.task('compile:styles', () => {
    let stream = gulp.src(options.styles.entry);

    if (!argv.compress) {
        stream = stream.pipe(sourcemaps.init());
    }

    stream = stream.pipe(sass({ outputStyle: (argv.compress ? 'compressed' : 'nested') })).on('error', logError);
    stream = stream.pipe(autoprefixer('last 2 versions')).on('error', logError);
    stream = stream.pipe(concat(options.styles.name)).on('error', logError);

    if (!argv.compress) {
        stream = stream.pipe(sourcemaps.write(options.maps.build)).on('error', logError);
    }

    return stream.pipe(gulp.dest(options.styles.build));
});

gulp.task('compile:scripts', () => {
    let stream = gulp.src(options.scripts.entry);

    if (!argv.compress) {
        stream = stream.pipe(sourcemaps.init());
    }

    stream = stream.pipe(concat(options.scripts.name));

    if (argv.compress) {
        stream = stream.pipe(uglify()).on('error', logError);
    } else {
        stream = stream.pipe(sourcemaps.write(options.maps.build)).on('error', logError);
    }

    return stream.pipe(gulp.dest(options.scripts.build));
});

gulp.task('optimise:images', () => {
    let paths = options.images.formats.map(format => {
        return options.images.path + '**/*.' + format;
    });

    let stream = gulp.src(paths).pipe(imageop(options.images.options)).on('error', error => {
        util.log(colors.red('Unable to optimise images'));
        this.emit('end');
    });

    return stream.pipe(gulp.dest(options.images.path));
});

gulp.task('watch:styles', () => gulp.watch(options.styles.watch, ['compile:styles']));
gulp.task('watch:scripts', () => gulp.watch(options.scripts.entry, ['compile:scripts']));
gulp.task('watch:images', function() {
    let paths = options.images.formats.map(format => {
        return options.images.path + '**/*.' + format;
    });

    gulp.watch(paths).on('change', file => {
        gulp.src(file.path).pipe(imageop(options.images.options)).on('error', error => {
            util.log(colors.red('Failed to optimise ' + file.path.split('/').pop()));
            this.emit('end');
        });
    });
});

gulp.task('compile', ['compile:styles', 'compile:scripts', 'optimise:images']);
gulp.task('watch', ['watch:styles', 'watch:scripts', 'watch:images']);

const logError = (error) => {
    let errorObject = JSON.parse(JSON.stringify(error));
    let notification = {
        title: 'Compilation error'
    };

    util.log(colors.red('COMPILATION ERROR'));

    if (Object.keys(errorObject).length > 0) {
        console.log('File: %s', errorObject.relativePath);
        console.log('Line: %s:%s', error.line, errorObject.column);
        console.log(errorObject.message.replace(errorObject.relativePath + "\n", ''));
        notification.message = errorObject.relativePath + ' on line ' + errorObject.line + ':' + errorObject.column;
    } else {
        util.log(error);
    }

    notifier.notify(notification);
    this.emit('end');
};